Initial Folder Structure Setup
-----
[Setup Folders]

Folders

`
    mkdir
    www
    www/public
    www/public/scss 
    www/public/js
    www/public/images
`

Files  
`
    touch
    .gitignore
    gulpfile.js
    www/public/index.html
    www/public/scss/main.scss
    www/public/js/script.js
`